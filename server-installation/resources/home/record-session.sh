#!/bin/bash

#    This file is part of Secure Access Environment.
#    Copyright (C) 2018 TUM/MRI
#
#    Secure Access Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Secure Access Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Secure Access Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Martin Herrmann, Helmut Spengler

# Record this session
RESOLUTION=$(xdpyinfo | grep -i dimensions: | sed 's/[^0-9]*pixels.*(.*).*//' | sed 's/[^0-9x]*//')
FRAMERATE=4
RECORDING_FNAME="/tmp/audit/session-$(date +%Y%m%d%H%M%S).avi"
ffmpeg -f x11grab -s $RESOLUTION -framerate $FRAMERATE -i $DISPLAY $RECORDING_FNAME &
