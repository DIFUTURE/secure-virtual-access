#!/usr/bin/env python3

#    This file is part of Secure Access Environment.
#    Copyright (C) 2018 TUM/MRI
#
#    Secure Access Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Secure Access Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Secure Access Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Martin Herrmann, Helmut Spengler

import logging
import os
import subprocess
import sys

import time

xdotool = "/usr/bin/xdotool"

# The list containing all allowed keys. Upper-case keys are automatically allowed but not the other way around.
# Names for the keys can be found (on Ubuntu systems) in /usr/include/X11/keysymdef.h
# The leading XK_ is not part of the key name (e.g. 'XK_Home' becomes just 'Home').
allowed_keys = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
                "v", "w", "x", "y", "z", "adiaresis", "odiaresis", "udiaresis", "ssharp", "0", "1", "2", "3", "4", "5",
                "6", "7", "8", "9", "space", "exclam", "quotedbl", "section", "dollar", "percent", "ampersand", "slash",
                "parenleft", "parenright", "equal", "question", "dead_acute", "dead_grave", "plus", "numbersign",
                "minus", "less", "greater", "period", "colon", "underscore", "comma", "semicolon", "asterisk",
                "apostrophe", "mu", "at", "EuroSign", "bar", "twosuperior", "threesuperior", "bracketright",
                "bracketleft", "braceright", "braceleft", "backslash", "asciitilde", "degree", "dead_circumflex",
                "Delete", "BackSpace", "Return", "F5", "Up", "Down", "Right", "Left", "Home", "End", "Prior", "Next"]

# Enforced timeout between two keystrokes in seconds
timeout = 0.02

last_key_stroke = time.time()
pushed_keys = set()

def handleEvent(raw, env):
    if raw.startswith("Keysym"):
        performKeyEvent(raw, env)
    elif raw.startswith("Pointer"):
        performMouseEvent(raw, env)
    elif raw.startswith("#"):
        logging.getLogger(__name__).debug(raw)
    else:
        logging.getLogger(__name__).error("Class unknown: " + raw)


def performKeyEvent(raw, env):
    args = [xdotool]

    list = raw.split()
    try:
        # check whether to press or release
        if list[5] == "KeyPress":
            args.append("keydown")
        elif list[5] == "KeyRelease":
            args.append("keyup")
        else:
            return

        # clear all modifiers. even shift is unnecessary
        args.append("--clearmodifiers")

        # check if the key is allowed
        key = list[4]
        if key in allowed_keys or key.lower() in allowed_keys:
            args.append(key)
        else:
            if key is not None:
                logging.getLogger(__name__).warning("Illegal key blocked: " + key)
            return

        # perform the key event
        global last_key_stroke
        if (time.time() - timeout) >= last_key_stroke or key in pushed_keys:
            subprocess.run(args, env=env)
            logging.getLogger(__name__).debug(args)
            if list[5] == "KeyPress":
                last_key_stroke = time.time()
                pushed_keys.add(key)
            else:
                pushed_keys.remove(key)
            # log
            logging.getLogger(__name__).info(list[5] + ": " + key)
        else:
            logging.getLogger(__name__).warning("Key blocked due to rate-limiting: " + key)
        
    except Exception as e:
        logging.getLogger(__name__).debug(raw)
        logging.getLogger(__name__).debug(e)
        return


def performMouseEvent(raw, env):
    args_click = [xdotool]
    args_move = [xdotool]

    list = raw.split()
    try:
        # move the mouse
        x = list[2]
        y = list[3]
        args_move.append("mousemove")
        args_move.append(str(x))
        args_move.append(str(y))
        subprocess.run(args_move, env=env)
        logging.getLogger(__name__).debug(args_click)

        # check if the button is allowed
        button = list[5]
        if button == "ButtonPress-1":
            args_click.append("mousedown")
            args_click.append("1")
        elif button == "ButtonRelease-1":
            args_click.append("mouseup")
            args_click.append("1")
        elif button == "ButtonPress-4":
            args_click.append("mousedown")
            args_click.append("4")
        elif button == "ButtonRelease-4":
            args_click.append("mouseup")
            args_click.append("4")
        elif button == "ButtonPress-5":
            args_click.append("mousedown")
            args_click.append("5")
        elif button == "ButtonRelease-5":
            args_click.append("mouseup")
            args_click.append("5")
        else:
            if button is not None and button != "None":
                logging.getLogger(__name__).warning("Illegal click blocked at (" + x + "," + y + "): " + button)
            return

        # perform the click event
        subprocess.run(args_click, env=env)

        # log
        logging.getLogger(__name__).info(button + " at (" + x + "," + y + ")")
        logging.getLogger(__name__).debug(args_click)
    except Exception as e:
        logging.getLogger(__name__).debug(raw)
        logging.getLogger(__name__).debug(e)
        return


if __name__ == '__main__':
    # setup the actual Logger
    log = logging.getLogger(__name__)
    log.setLevel(logging.INFO)
    handler = logging.FileHandler("/tmp/audit/strokelog", 'a', "UTF-8")
    handler.setFormatter(logging.Formatter('%(asctime)s [%(levelname)s] %(message)s'))
    log.addHandler(handler)

    # setup environment
    env = os.environ.copy()
    env["DISPLAY"] = ":20.0"
    env["XAUTHORITY"] = "/home/browser/.Xauthority"

    # select the active window
    subprocess.run([xdotool, "getactivewindow"])

    # start loop
    try:
        buffer = ""
        while True:
            buffer += sys.stdin.read(1)
            if buffer.endswith("\n"):
                handleEvent(buffer[:-1], env)
                buffer = ""
    except KeyboardInterrupt:
        pass
