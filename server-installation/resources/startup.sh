#!/bin/bash

#    This file is part of Secure Access Environment.
#    Copyright (C) 2018 TUM/MRI
#
#    Secure Access Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Secure Access Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Secure Access Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Martin Herrmann, Helmut Spengler

# change password of user 'browser'
echo "browser:$USER_PW" | chpasswd
unset unset USER_PW

# change target URL
sed -e "s|http://etriks1.uni.lu/transmart/datasetExplorer|$TARGET_URL|" -i .bak /etc/apache2/sites-enabled/default-ssl.conf

# start supervisord which starts the necessary daemons
supervisord -c /etc/supervisor.conf
