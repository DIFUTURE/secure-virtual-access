#!/bin/bash

#    This file is part of Secure Access Environment.
#    Copyright (C) 2018 TUM/MRI
#
#    Secure Access Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Secure Access Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Secure Access Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Martin Herrmann, Helmut Spengler

# Create CA
openssl req -batch -x509 -config openssl-ca.cnf -newkey rsa:4096 -sha256 -nodes -out cacert.pem -outform PEM
certutil -d sql:/home/browser/.pki/nssdb -A -t "C,," -n "DIFUTURE CA" -i cacert.pem

# Create CSR
openssl req -batch -config localhost.cnf -new -sha256 -newkey rsa:2048 -nodes -keyout /etc/ssl/private/local.key.pem -out localhost.req.pem

# Sign CSR
touch index.txt
echo '01' > serial.txt
openssl ca -batch -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out /etc/ssl/certs/local.cert.pem -infiles localhost.req.pem

# Clean up
rm index.txt*
rm serial.txt*
rm localhost.req.pem
rm cakey.pem cacert.pem
rm 01.pem
