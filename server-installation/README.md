This directory contains:
* `audit` (directory): per default, the proxy server is configured to store its audit information (i.e. strokelog, screen-recordings) here
* `resources` (directory): contains all files necessary to build the Docker container containing the proxy-server
* `.env` : contains the runtime configuration for the proxy-server
* `Dockerfile` (file): contains the build instructions for the Docker image
* `docker-compose.yml` (file): serves as a wrapper for the runtime configuration to the `docker-compose` tool
