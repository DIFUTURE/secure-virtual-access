# Table of contents
1. [Introduction](#introduction)
2. [Architecture](#architecture)
3. [Server operation](#server-operation)
4. [Client operation](#client-operation)

# Introduction
Controlled data access environments enable data custodians to share data in a safe setting while maintaining oversight. This project contains an implementation of a controlled data access environment which can be used for providing researchers with access to a modern web-based analytics platforms for biomedical data. Examples include i2b2 or tranSMART. The controlled access environment is created by installing a proxy between the client and the server hosting the analytics software. 

The proxy provides a way of controlling the interactions between the analyst and the analysis software. All information displayed to the data recipient by the analytics software is only available as unstructured graphical data in form of a (compressed) stream of images. The permitted user interactions, i.e. mouse and keyboard events, can be restricted by the proxy and are being redirected to the analytics platform. Special care has been taken to close a wide variety of potential side channels. The proxy is configured to not support copy-and-paste operations between the client and the server to prevent the user from transferring structured data. In order to prevent the user from uploading significant amounts of data via simulated keyboard interactions, rate limiting has been put in place. As a cross-sectional mitigation strategy, all keyboard interactions are logged and a video of the screen content during interactive sessions is recorded.

## Intended audience
This documentation is intended for system architects and administrators responsible for integrating this solution into the IT environment of an organization. It is **not** intended for end users.

# Architecture

![Architecture][architecture]

[architecture]: img/architecture.png "Architecture"

As is shown in the figure above, the proxy provides a way of controlling the interactions between the analyst and the analysis software by exposing its frontend via Virtual Network Computing (VNC) using the Remote Framebuffer (RFB) protocol. The VNC server acts as terminal, which provides only the contents of its framebuffer to the VNC client. This means that to the data recipient all information displayed by the analytics software is only available as unstructured graphical data in form of a (compressed) stream of images.

The VNC client accepts and transfers user input, i.e. mouse and keyboard events, to the VNC server. The VNC server redirects these events to an X-Server, which also provides the framebuffer that is exposed by the VNC server. As a bridge between the proxy and the analytics software, the X-Server is configured to execute exactly one predefined application: a web browser running in kiosk mode which is configured to show the interface of the analytics platform only.

## Components
The figure below shows the individual components of the controlled data access environment in more detail.

![Component architecture][component-architecture]

[component-architecture]: img/component-architecture.png "Component architecture"

* **application-server**: An arbitrary web-application server providing access to an application with a HTML frontend.
* **proxy-server**: The central part of the component architecture provides controlled access to the application server for the clients. Its sub-components are encapsulated in a *Docker* container.
  * **terminal-server** A VNC server which acts as a terminal. It presents only the contens of its framebuffer to the VNC client. It accepts mouse and keyboard events and transmits them to the input filter. In order to prevent the user from transferring structured data, the server is configured to not support copy-and-paste operations between the terminal-client and the server components. 
  * **input filter**: In order to prevent the user from uploading significant amounts of data via simulated keyboard interactions, this component realizes rate limiting for keyboard events. Furthermore, only a predefined set of keys is transmitted to the server, prohibiting the execution of control key shortcuts (e.g. *CTRL-N*).
  * **Xserver**: Provides the framebuffer that is exposed by the terminal-server. As a bridge between the terminal-server and the application-server, it is configured to execute exactly one predefined application: a Chromium web broser running in kiosk mode which is configured to show the interface of the application-server only. The user is securely locked into the browser and no other programs can be executed by the user. The browser can only communicate with the application-server via an Apache2 reverse proxy.
  * **Apache2 reverse proxy**: An important reqirement was that the URL of the application-server can be configured at runtime. However, the configuration of the kiosk URL of the web browser cannot be defined neither via a configuration file nor as command line parameter. As a solution, we point the kiosk to `https://localhost`, which resolves to the Apache2 reverse proxy described here. We utilitze this proxy's configuration file for programmatically changing the URL of the application server at runtime. This approach has the additional advantage that communication can be further restricted, if necessary.
  * **Screen-logging**: A video of the screen content during interactive sessions is recorded to a configurable directory.
  * **Keyboard-logging**: Furthermore, all keyboard strokes are logged to a configurable directory.
* **terminal-client**: An arbitrary VNC client. It accepts and transfers user input i.e. mouse and keyboard events to the terminal-server.

# Server operation
## System requirements
* Supported operating systems
  * Linux
  * Windows 7 or higher
* Required software
  * *docker* (Version 18 or higher, respectively).
    * Linux: please use your package manager for installation and install the package *docker*
    * Windows: please use these links for [Windows 10](https://store.docker.com/editions/community/docker-ce-desktop-windows) or for [older versions of Windows](https://docs.docker.com/toolbox/overview/)
  * *docker-compose* 1.22.0 or higher
    * Linux: as some package repositories (e.g. those for Ubuntu) only provide outdated versions of *docker-compose*, we recommend obtaining it directly from [the docker repository](https://docs.docker.com/compose/install/)
    * Windows: an up-to-date version of *docker-compose* is typically included in the installation of *docker*

## Installation
Execute following commands as a privileged user:

```bash
cd server-installation
docker-compose up -d
```
* Note down the MD5 fingerprint of the SSH server's RSA public key, which is printed near the end of the build process. This key is needed by the client for verifying the server's identity when connecting.
* Edit the variable `USER_PW` in the configuration file `.env` to contain the password which should be used by the client when establishing the SSH connection.

Note that on Windows 7 and 8, the IP address of the Docker guests (e.g. the proxy-server) will differ from the IP address of the host. This address is displayed on startup of the *Docker Quickstart Terminal*. Alternatively, you can query the IP with the command `docker-machine ip` from within the *Docker Quickstart Terminal* terminal. This IP needs to be configured when installing the client.

## Configuration
Most configuration options for the server can be set in the file `server-installation/.env`
* `USER_PW` In order to facilitate a smooth setup for an easy evaluation of the environment, the password which should be used by the client when establishing the SSH connection, can be stored here. **Important note:** This authentication mechanism is not intended for production environments, where public key authentication instead of password authentication should be used.
* `SSH_PORT` The port on which the SSH server should listen for connection requests. This is likely *not* the standard SSH port (because you will typically need it for accessing the Docker host).
* `TARGET_URL` The URL of the application which should be presented by the terminal.
* `AUDIT_DIR` The directory containing
  * Video recordings of user sessions.
  * Logs of the keystrokes performed by users.

Moreover, the variable `allowed_keys` in the file `server-installation/home/keystrokes.py` contains a whitelist of keys that will be accepted. Only keys appearing in this list will be transmitted to the application server / analytics. All other keystrokes will be blocked.

# Client operation

## System requirements
* No restrictions regarding the client operation system, as long as the system requirements are fulfilled.
* SSH client that is capable of opening SSH tunnels (e.g. [OpenSSH](https://www.openssh.com/), [PLINK](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html))
* VNC client (e.g. [TightVNC](https://www.tightvnc.com/download.php), [UltraVNC](https://www.uvnc.com/downloads/ultravnc.html), [RealVNC](https://www.realvnc.com/en/connect/download/viewer/)).

## Accessing the environment

1. Create an SSH tunnel to the proxy-server as user `browser` using 5900 as local and as destination port (e.g. `ssh -p 2222 -L 5900:localhost:5900 -l browser <proxy-server-address>`)
2. Open your VNC viewer and point it to the tunnel (e.g. `vncviewer localhost`)

# License
Licensed under the GNU GPL-3.0-only (the "License"); you may not use this software except in compliance with the License. You may obtain a copy of the License at

https://www.gnu.org/licenses/gpl-3.0.en.html .

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

# References
Prasser F, Kohlmayer F, Spengler H, Kuhn KA. *A Scalable and Pragmatic Method for the Safe Sharing of High-Quality Health Data*. IEEE J Biomed Health Inform. 2018;22(2):611-622.